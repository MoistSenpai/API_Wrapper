const request = require('request');

class Main {
    constructor(key) {
        if (!key || key == '' || key === undefined) throw 'No API key was provided!';
        this.key = key;
    }

    // ----------------------------------------

    youtubeSearch(search, options) {
        let key = this.key;

        return new Promise((resolve, reject) => {
            if (!search || search == '' || search === undefined)
                return reject('No search querry provided!');
            else if (!options || options === undefined) options = {
                maxResults: 15
            };
            else if (options && !options.maxResults)
                return reject('No maxResults property in options!');

            request(`http://quarkz.xyz/api/yt_video?maxResults=${options.maxResults}&search=${search}&key=${key}`, (err, resp, bod) => {
                try {
                    if (err)
                        return reject(err);
                    else if (bod.startsWith('<!DOCTYPE HTML PUBLIC'))
                        return reject(`Service unavailable`);
                    else if (bod.startsWith('Too many requests'))
                        return reject(`Too many requests, please try agin later.`);
                    bod = JSON.parse(bod);

                    if (bod.state === 'fail')
                        return reject(`Err ${bod.status} ${bod.message}`);
                    resolve(bod);
                } catch (err) {
                    reject(err);
                }
            });

        });

    }

    // ----------------------------------------

    youtubePlaylistSearch(id, options) {
        let key = this.key;

        return new Promise((resolve, reject) => {
            if (!id || typeof id === '' || id === undefined)
                return reject('No playlist ID provided!');
            else if (!options || options === undefined) options = {
                maxResults: 15
            };
            else if (options && !options.maxResults)
                return reject('No maxResults property in options!');

            request(`http://quarkz.xyz/api/yt_playlist?id=${id}&maxVideos=${options.maxResults}&key=${key}`, (err, resp, bod) => {
                if (err)
                    return reject(err);
                try {
                    if (bod.startsWith('<!DOCTYPE HTML PUBLIC'))
                        return reject(`Service unavailable`);
                    else if (bod.startsWith('Too many requests'))
                        return reject(`Too many requests, please try agin later.`);
                    bod = JSON.parse(bod);
                    if (bod.state === 'fail')
                        return reject(`Err ${bod.status} ${bod.message}`);
                    resolve(bod);
                } catch (err) {
                    reject(err);
                }
            });

        });

    }

    // ----------------------------------------

    getPlaylistID(link) {
        let regex = /[?&]list=([^#\&\?]+)/;

        if (!link) throw 'No link provided!';
        else if (!regex.exec(link)) throw `Unable to gather playlist ID from ${link}`;
        else
            return regex.exec(link)[1];
    }

    // ----------------------------------------

    getVideoID(link) {
        let regex = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;

        if (!link) throw 'No link provided!';
        else if (!regex.exec(link)) throw `Unable to gather video ID from ${link}`;
        else
            return regex.exec(link)[2];
    }

}

module.exports = Main;
