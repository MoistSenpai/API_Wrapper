const config = require('./config.json');
const App = require('./index.js');
const app = new App(config.key);

app.youtubeSearch('Minecraft', { maxResults: 2 }).then((bod) => {
    console.log(JSON.stringify(bod.result[0].video.title, null, ' '));
}).catch((err) => console.log(`Error: ${err}`));
