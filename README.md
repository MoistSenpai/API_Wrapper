# Nep API Wrapper

Simple wrapper built for the Nep API.

## Getting Started

`npm i --save neptune-api-wrapper`

## Search for YouTube Videos
* string **Search query paramiter**
* object **maxResults**

```js
const Nep = require('neptune-api-wrapper');
const nep = new Nep('API Key');

nep.youtubeSearch('despacito').then((res) => {
    console.log( JSON.stringify(res) );
}).catch((err) => console.log(err) );
```
**If there was no maxResults, the default is set to 15**

```js
nep.youtubeSearch('despacito', {maxResults: 5}).then((res) => {
    // ...
    // output https://pastebin.com/3Ri6EqNT
}).catch((err) => console.log(err) );
```

## Getting Videos from Playlist
* string **YouTube Playlist ID**

```js
let playlistID = 'RDEMw4pUX9POHNA4NYoqgKAsuw';

nep.youtubePlaylistSearch(playlistID).then((res) => {
    // ...
    // output https://pastebin.com/JpefuAwT
}).catch((err) => console.log(err) );
```

## Get Playlist ID
* string **YouTube Playlist Link**

```js
let link = `https://www.youtube.com/watch?v=wrNTOo4KH8c&list=RDEMw4pUX9POHNA4NYoqgKAsuw&index=5`;
let id = nep.getPlaylistID(link);

console.log(id); // RDEMw4pUX9POHNA4NYoqgKAsuw
```

## Get ID from YouTube video
* string **YouTube video link**

```js
let link = `https://www.youtube.com/watch?v=dQw4w9WgXcQ`;
let id = nep.getVideoID(link);

console.log(id); // v=dQw4w9WgXcQ
```

## Author
* **MoistSenpai**